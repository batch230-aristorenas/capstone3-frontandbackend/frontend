import {useContext, useEffect, useState} from "react";
import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import Swal from "sweetalert2";
import UserDetails from "./UserDetails";

import UserContext from "../UserContext";

export default function AdminDashboard(){

    const { user } = useContext(UserContext);
    const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));

    const [allProduct, setAllProduct] = useState([]);

    const [productId, setProductId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(0);

    const [isActive, setIsActive] = useState(false);

    const [showUpdate, setShowUpdate] = useState(false);
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

    const openAdd = () => setShowAdd(true); //Will show the modal
	const closeAdd = () => setShowAdd(false); //Will hide the modal

    const openUpdate = () => setShowUpdate(true); //Will show the modal
	const closeUpdate = () => setShowUpdate(false); //Will hide the modal

    const openEdit = (id) => {
		setProductId(id);

		// Getting a specific product to pass on the edit modal
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			// updating the product states for editing
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStock(data.stock);
		});

		setShowEdit(true)
	};

    const closeEdit = () => {

		// Clear input fields upon closing the modal
	    setName('');
	    setDescription('');
	    setPrice(0);
	    setStock(0);

		setShowEdit(false);
	};

    // Fetch all products
    const fetchData = () =>{
		// get all the products from the database
		fetch(`${process.env.REACT_APP_API_URL}/products`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProduct(data.map(product => {
				return (
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stock}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								// conditonal rendering on what button should be visible base on the status of the product
								(product.isActive)
								?
									<Button variant="danger" size="sm" onClick={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.name)}>Unarchive</Button>
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	}

	// to fetch all products in the first render of the page.
    useEffect(()=>{
		fetchData();
	}, [])

    // Making the product inactive
	const archive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${productName} is now inactive.`
				});
				// To show the update with the specific operation initiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

    // Making the product active
	const unarchive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${productName} is now active.`
				});
				// To show the update with the specific operation initiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}




const addProduct = (event) =>{
    // Prevents page redirection via form submission
    event.preventDefault();

    fetch(`${ process.env.REACT_APP_API_URL }/products/create`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            name: name,
            description: description,
            price: price,
            stock: stock
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        if(data){
            Swal.fire({
                title: "Product Succesfully Added",
                icon: "success",
                text: `${name} is now added`
            });

            // To automatically add the update in the page
            //fetchData();
            // Automatically closed the modal
            closeAdd();
        }
        else{
            Swal.fire({
                title: "Error!",
                icon: "error",
                text: `Something went wrong. Please try again later!`
            });
            closeAdd();
        }

    })

    // Clear input fields
    setName('');
    setDescription('');
    setPrice(0);
    setStock(0);
}

// edit a specific product
const editProduct = (e) =>{
            // Prevents page redirection via form submission
            e.preventDefault();

            fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    name: name,
                    description: description,
                    price: price,
                    stock: stock
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if(data){
                    Swal.fire({
                        title: "Product succesfully Updated",
                        icon: "success",
                        text: `${name} is now updated`
                    });

                    // To automatically add the update in the page
                    fetchData();
                    // Automatically closed the form
                    closeEdit();

                }
                else{
                    Swal.fire({
                        title: "Error!",
                        icon: "error",
                        text: `Something went wrong. Please try again later!`
                    });

                    closeEdit();
                }

            })

            // Clear input fields
            setName('');
            setDescription('');
            setPrice(0);
            setStock(0);
    } 

    // Submit button validation for add/edit course
    useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
        if(name != "" && description != "" && price > 0 && stock > 0){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price, stock]);





return(
    (userRole)
    ?
    <>
    <div id="adminContainer">
        {/*Header for the admin dashboard and functionality for create product and show enrollments*/}
        <div className="mt-5 mb-3 text-center">
            <h1>Admin Dashboard - Product Details</h1>
            {/*Adding a new product */}
            <Button variant="light" className="
            mx-2" onClick={openAdd}>Add Product</Button>
            {/*Route to UserDetails Page*/}
            <Button as={Link} to="/admin/UserDetails" variant="light" className="
            mx-2">User Details</Button>
        </div>
        {/*End of admin dashboard header*/}

        {/*For view all the products in the database.*/}
        <Table id="adminContent">
            <thead>
            <tr>
                <th>Product ID</th>
                <th>Product Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Stocks</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            {allProduct}
            </tbody>
        </Table>
			{/*End of table for product viewing*/}

{/*Modal for Adding a new product*/}
        <Modal id='addModalContainer' show={showAdd} fullscreen={true} onHide={closeAdd}>
            <Form id='addModalContent' onSubmit={e => addProduct(e)}>

                <Modal.Header closeButton>
                    <Modal.Title>Add New Product</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form.Group controlId="name" className="mb-3">
                        <Form.Label >Product Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Enter Product Name" 
                            value = {name}
                            onChange={e => setName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="description" className="mb-3">
                        <Form.Label>Product Description</Form.Label>
                        <Form.Control
                            as="textarea"
                            rows={3}
                            placeholder="Enter Product Description" 
                            value = {description}
                            onChange={e => setDescription(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="price" className="mb-3">
                        <Form.Label>Product Price</Form.Label>
                        <Form.Control 
                            type="number" 
                            placeholder="Enter Product Price" 
                            value = {price}
                            onChange={e => setPrice(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="slots" className="mb-3">
                        <Form.Label>Product Stocks</Form.Label>
                        <Form.Control 
                            type="number" 
                            placeholder="Enter Product Stocks" 
                            value = {stock}
                            onChange={e => setStock(e.target.value)}
                            required
                        />
                    </Form.Group>
                </Modal.Body>

                <Modal.Footer>
                    { isActive 
                        ? 
                        <Button variant="primary" type="submit" id="submitBtn">
                            Save
                        </Button>
                        : 
                        <Button variant="danger" type="submit" id="submitBtn" disabled>
                            Save
                        </Button>
                    }
                    <Button variant="secondary" onClick={closeAdd}>
                        Close
                    </Button>
                </Modal.Footer>

            </Form>	
        </Modal>
    {/*End of modal for adding product*/}

    {/*Modal for Editing a product*/}
    <Modal id='editModalContainer' show={showEdit} fullscreen={true} onHide={closeEdit}>
        <Form id='editModalContent' onSubmit={e => editProduct(e)}>

            <Modal.Header closeButton>
                <Modal.Title>Edit a Product</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Form.Group controlId="name" className="mb-3">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter Product Name" 
                        value = {name}
                        onChange={e => setName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="description" className="mb-3">
                    <Form.Label>Product Description</Form.Label>
                    <Form.Control
                        as="textarea"
                        rows={3}
                        placeholder="Enter Product Description" 
                        value = {description}
                        onChange={e => setDescription(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="price" className="mb-3">
                    <Form.Label>Product Price</Form.Label>
                    <Form.Control 
                        type="number" 
                        placeholder="Enter Product Price" 
                        value = {price}
                        onChange={e => setPrice(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="slots" className="mb-3">
                    <Form.Label>Product Slots</Form.Label>
                    <Form.Control 
                        type="number" 
                        placeholder="Enter Product Slots" 
                        value = {stock}
                        onChange={e => setStock(e.target.value)}
                        required
                    />
                </Form.Group>
            </Modal.Body>

            

            <Modal.Footer>
                { isActive 
                    ? 
                    <Button variant="danger" type="submit" id="submitBtn">
                        Save
                    </Button>
                    : 
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                        Save
                    </Button>
                }
                <Button variant="secondary" onClick={closeEdit}>
                    Close
                </Button>
            </Modal.Footer>

        </Form>	
    </Modal>
    </div>

</>
    :
    <Navigate to="/products" />
)
}